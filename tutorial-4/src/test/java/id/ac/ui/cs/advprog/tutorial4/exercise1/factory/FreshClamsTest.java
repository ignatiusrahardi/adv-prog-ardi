package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;


import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FreshClamsTest {

    @Test
    public void testToString() {
        FreshClams clam = new FreshClams();
        String equal = "Fresh Clams from Long Island Sound";
        assertEquals(equal, clam.toString());
    }
}
