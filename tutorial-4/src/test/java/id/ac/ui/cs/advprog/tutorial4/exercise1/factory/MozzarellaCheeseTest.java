package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MozzarellaCheeseTest {
    @Test
    public void testToString() {
        MozzarellaCheese mozzarellaCheese = new MozzarellaCheese();
        String equal = "Shredded Mozzarella";
        assertEquals(equal, mozzarellaCheese.toString());
    }

}
