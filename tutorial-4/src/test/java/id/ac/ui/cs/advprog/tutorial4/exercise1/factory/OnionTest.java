package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;


import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class OnionTest {

    @Test
    public void testToString() {
        Onion veggie = new Onion();
        String equal = "Onion";
        assertEquals(equal, veggie.toString());
    }

}
