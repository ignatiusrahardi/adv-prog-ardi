package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BechamelSauce;
import org.junit.Test;
import static org.junit.Assert.assertEquals;



public class BechamelSauceTest {

    @Test
    public void testToString() {
        BechamelSauce sauce = new BechamelSauce();
        String equal = "Béchamel Sauce";
        assertEquals(equal, sauce.toString());
    }


}
