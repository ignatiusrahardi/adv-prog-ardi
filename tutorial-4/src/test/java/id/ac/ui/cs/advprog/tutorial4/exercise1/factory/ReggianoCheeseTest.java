package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ReggianoCheeseTest {

    @Test
    public void testToString() {
        ReggianoCheese reggianoCheese = new ReggianoCheese();
        String equal = "Reggiano Cheese";
        assertEquals(equal, reggianoCheese.toString());
    }

}
