package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class ThinCrustDoughTest {

    @Test
    public void testToString() {
        ThinCrustDough dough = new ThinCrustDough();
        String equal = "Thin Crust Dough";
        assertEquals(equal, dough.toString());
    }


}
