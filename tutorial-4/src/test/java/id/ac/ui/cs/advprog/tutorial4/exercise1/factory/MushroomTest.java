package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MushroomTest {


    @Test
    public void testToString() {
        Mushroom veggie = new Mushroom();
        String equal = "Mushrooms";
        assertEquals(equal, veggie.toString());
    }
}
