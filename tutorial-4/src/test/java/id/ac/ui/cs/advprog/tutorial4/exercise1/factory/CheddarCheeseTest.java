package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CheddarCheeseTest {
    @Test
    public void testToString() {
        CheddarCheese cheddarCheese = new CheddarCheese();
        String equal = "Cheddar Cheese";
        assertEquals(equal, cheddarCheese.toString());
    }

}
