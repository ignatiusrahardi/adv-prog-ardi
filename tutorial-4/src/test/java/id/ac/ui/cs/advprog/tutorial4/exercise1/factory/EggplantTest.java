package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class EggplantTest {

    @Test
    public void testToString() {
        Eggplant veggie = new Eggplant();
        String equal = "Eggplant";
        assertEquals(equal, veggie.toString());
    }

}
