package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class BlackOlivesTest {

    @Test
    public void testToString() {
        BlackOlives veggie = new BlackOlives();
        String equal = "Black Olives";
        assertEquals(equal, veggie.toString());
    }
}
