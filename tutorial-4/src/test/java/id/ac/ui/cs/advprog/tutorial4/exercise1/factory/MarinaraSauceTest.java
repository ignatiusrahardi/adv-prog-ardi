package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class MarinaraSauceTest {

    @Test
    public void testToString() {
        MarinaraSauce sauce = new MarinaraSauce();
        String equal = "Marinara Sauce";
        assertEquals(equal, sauce.toString());
    }

}
