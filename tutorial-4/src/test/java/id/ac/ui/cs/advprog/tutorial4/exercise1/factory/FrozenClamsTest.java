package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FrozenClamsTest {

    @Test
    public void testToString() {
        FrozenClams clam = new FrozenClams();
        String equal = "Frozen Clams from Chesapeake Bay";
        assertEquals(equal, clam.toString());
    }


}
