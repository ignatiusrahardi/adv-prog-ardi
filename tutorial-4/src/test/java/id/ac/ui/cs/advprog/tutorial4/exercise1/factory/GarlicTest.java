package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class GarlicTest {

    @Test
    public void testToString() {
        Garlic veggie = new Garlic();
        String equal = "Garlic";
        assertEquals(equal, veggie.toString());
    }


}
