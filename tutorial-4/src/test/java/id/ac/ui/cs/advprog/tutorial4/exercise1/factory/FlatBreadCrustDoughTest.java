package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.FlatBreadCrustDough;
import org.junit.Test;
import static org.junit.Assert.assertEquals;



public class FlatBreadCrustDoughTest {

    @Test
    public void testToString() {
        FlatBreadCrustDough dough = new FlatBreadCrustDough();
        String equal = "Flat Bread Crust Dough";
        assertEquals(equal, dough.toString());
    }

}
