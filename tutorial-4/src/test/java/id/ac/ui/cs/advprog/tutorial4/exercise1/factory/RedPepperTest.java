package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RedPepperTest {

    @Test
    public void testToString() {
        RedPepper veggie = new RedPepper();
        String equal = "Red Pepper";
        assertEquals(equal, veggie.toString());
    }

}
