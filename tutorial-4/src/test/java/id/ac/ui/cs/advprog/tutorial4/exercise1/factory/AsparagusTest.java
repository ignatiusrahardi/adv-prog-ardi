package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Asparagus;
import org.junit.Test;
import static org.junit.Assert.assertEquals;



public class AsparagusTest {

    @Test
    public void testToString() {
        Asparagus veggie = new Asparagus();
        String equal = "Asparagus";
        assertEquals(equal, veggie.toString());
    }
}
