package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    protected PizzaStore pizzaStore;

    @Before
    public void setUp() {
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza;
        pizza = pizzaStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        pizza = pizzaStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        pizza = pizzaStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
    }
}