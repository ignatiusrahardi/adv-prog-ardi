package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ParmesanCheeseTest {

    @Test
    public void testToString() {
        ParmesanCheese parmesanCheese = new ParmesanCheese();
        String equal = "Shredded Parmesan";
        assertEquals(equal, parmesanCheese.toString());
    }

}
