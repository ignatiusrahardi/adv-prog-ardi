package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.WhiteClams;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class WhiteClamsTest {

    @Test
    public void testToString() {
        WhiteClams clam = new WhiteClams();
        String equal = "White Clams from Chesapeake Bay";
        assertEquals(equal, clam.toString());
    }

}
