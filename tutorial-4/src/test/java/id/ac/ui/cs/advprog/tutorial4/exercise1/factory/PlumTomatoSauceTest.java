package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class PlumTomatoSauceTest {

    @Test
    public void testToString() {
        PlumTomatoSauce sauce = new PlumTomatoSauce();
        String equal = "Tomato sauce with plum tomatoes";
        assertEquals(equal, sauce.toString());
    }

}
