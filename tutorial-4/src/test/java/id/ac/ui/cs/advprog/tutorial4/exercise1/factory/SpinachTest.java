package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SpinachTest {

    @Test
    public void testToString() {
        Spinach veggie = new Spinach();
        String equal = "Spinach";
        assertEquals(equal, veggie.toString());
    }


}
