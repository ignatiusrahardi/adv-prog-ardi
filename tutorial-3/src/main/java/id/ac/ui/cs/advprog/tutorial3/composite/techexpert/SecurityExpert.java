package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        this.name = name;
        if (salary < 70000) {
            throw new IllegalArgumentException("Invalid salary");
        }
        this.salary = salary;
    }


    public double getSalary() {
        return salary;
    }

    public String getRole() {
        return "Security Expert";
    }
}
