package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        this.food = food;
        //Done
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding chili sauce";
        //Done
    }

    @Override
    public double cost() {
        return food.cost() + 0.3;
        //Done
    }
}
