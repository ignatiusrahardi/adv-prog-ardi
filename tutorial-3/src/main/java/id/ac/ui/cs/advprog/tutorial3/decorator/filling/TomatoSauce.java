package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        this.food = food;
        //Done
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding tomato sauce";
        //Done
    }

    @Override
    public double cost() {
        return food.cost() + 0.2;
        //Done
    }
}
