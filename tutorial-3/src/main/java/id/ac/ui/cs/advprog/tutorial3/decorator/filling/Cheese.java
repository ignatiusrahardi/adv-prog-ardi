package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
        //Done
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding cheese";
        //Done
    }

    @Override
    public double cost() {
        return food.cost() + 2.00;
        //Done
    }
}
