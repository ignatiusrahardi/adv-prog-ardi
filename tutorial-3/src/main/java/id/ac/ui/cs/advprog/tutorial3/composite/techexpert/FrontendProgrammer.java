package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        if (salary < 30000) {
            throw new IllegalArgumentException("Invalid salary");
        }
        this.salary = salary;
    }


    public double getSalary() {
        return salary;
    }

    public String getRole() {
        return "Front End Programmer";
    }
}
