package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        this.food = food;
        //Done
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding lettuce";
        //Done
    }

    @Override
    public double cost() {
        return food.cost() + 0.75;
        //Done
    }
}
