import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();


            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" 
                   + String.valueOf(each.getCharge()) + "\n";
            totalAmount += each.getCharge();
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoint()) 
               + " frequent renter points";

        return result;
    }



    private double amountFor() {
        double total = 0;

        for (Rental each : rentals) {
            total += each.getCharge();
        }
        return total;


    }

    private int getTotalFrequentRenterPoint() {
        int total = 0;

        for (Rental each : rentals) {
            total = each.getFrequentRenterPoints(total);
        }
        return total;
    }

}