package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
	
	public ModelDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Quack();
		
	}
	
	public void display() {
		System.out.println("i'm a model duck");	
	}
	// TODO Complete me!
}
